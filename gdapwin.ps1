#######################################################################################################################################################
## Name             : ProvisionGDAPAnalyticsLabWinVM.ps1
## Date             : 26/07/2017
## Author           : Amalnath P A, Jomy Sebastian, Dhanesh D Lal
## Company          : EYC3
## Purpose          : Provision Windows Analytics Lab
## LLD Ref          : 
## Called by        : Urban Code. Set per subscription per resource type
#######################################################################################################################################################

Param(   
    [string] [Parameter(Mandatory=$true)] $ResourceGroupName, #Name of the Resource Group into which the analytics lab is deployed.
    [string] [Parameter(Mandatory=$true)] $ResourceGroupLocation, #Location into which the analytics lab is deployed.
    [string] [Parameter(Mandatory=$true)] $project_name, #Used for billing reporting. This name is used for PDDS project folder creation.
    [string] [Parameter(Mandatory=$true)] $project_id, #Used for billing reporting
    [string] [Parameter(Mandatory=$true)] $environment, #sit/dev/uat/prod (allowed values)
    [string] [Parameter(Mandatory=$true)] $category_role, #Used for billing reporting
    [string] [Parameter(Mandatory=$true)] $division, #ao/gcc/na/eo (allowed values)
    [string] [Parameter(Mandatory=$true)] $pii_use, #Used for billing reporting
    [string] [Parameter(Mandatory=$true)] $data_source_name, #Used for billing reporting
    [string] [Parameter(Mandatory=$true)] $business_unit, #Used for billing reporting
    [string] [Parameter(Mandatory=$true)] $contact_email, #Used for billing reporting
    [string] [Parameter(Mandatory=$true)] $expiration_date, #Used for billing reporting
	[string] [Parameter(Mandatory=$true)] $cost_center, #Used for billing reporting
    [string] [Parameter(Mandatory=$true)] $cataloguetype, #small/medium/large (size of analytics lab)
    [string] [Parameter(Mandatory=$true)] $vnetrg, #Resource group for vnet
    [string] [Parameter(Mandatory=$true)] $vnetname, #Name of the Virtual Network used by the VM
    [string] [Parameter(Mandatory=$true)] $Subnetname, #Name of the Subnet used by the VM
    [string] [Parameter(Mandatory=$true)] $vmname, #Name of the VM of the analytics lab
    [string] [Parameter(Mandatory=$true)] $domaintojoin, #QBE Active Directory Global Domain
    [string] [Parameter(Mandatory=$true)] $domainuser, #Service account of Global Domain
    [string] [Parameter(Mandatory=$true)] $domainpassword, #Domain Service Account Password
    [string] [Parameter(Mandatory=$true)] $oupath, #OU Path for Computer join
    [string] [Parameter(Mandatory=$true)] $groupoupath, #OU Path for AD Group creation
    [string] [Parameter(Mandatory=$true)] $domainjoinoptions,
    [string] [Parameter(Mandatory=$true)] $SAname, #Name of the Analytics Lab Storage Account
    [string] [Parameter(Mandatory=$true)] $SQLname, #Name of the Analytics Lab SQL Server name
    [string] [Parameter(Mandatory=$true)] $dwname, #Name of the Analytics Lab DW. Defaulted as labdw in GDAP
	[string] [Parameter(Mandatory=$true)] $adfname, #Name of the Azure Data Factory in Analytics Lab
    [string] [Parameter(Mandatory=$true)] $sharedtemplateurl, #common url path to shared templates
	[string] [Parameter(Mandatory=$true)] $existingRecoveryServicesVaultName, #Backup vault name
	[string] [Parameter(Mandatory=$true)] $existingBackupPolicyName, #Backup policy name
	[string] [Parameter(Mandatory=$true)] $LANID, #LANID of the person executing the provisioning. This person would be the Owner of the Analytics Lab
	[string] [Parameter(Mandatory=$true)] $userdomain, #Regional domain of the Owner (e.g.qbeau)
	[string] [Parameter(Mandatory=$true)] $AnalyticsLabADGroupName, #Active Directory group of the Analytics Lab
	[string] [Parameter(Mandatory=$true)] $AnalyticsLabAdminADGroupName, #Administrator Active Directory group of the Analytics Lab
	[string] [Parameter(Mandatory=$true)] $ITServerAdminsGroupName, #QBE Admin Groups
	[string] [Parameter(Mandatory=$true)] $ServerOperationsToolsGroupName, #QBE Admin Groups
	[string] [Parameter(Mandatory=$true)] $ServerEngineersNonProdGroupName, #QBE Admin Groups
	[string] [Parameter(Mandatory=$true)] $scomserver, #SCOM server to which the Analytics Lab VM is connected
	[string] [Parameter(Mandatory=$true)] $mediarg, #Media Library Resource Group Name
	[string] [Parameter(Mandatory=$true)] $mediasubscriptionid, #Media Library Subscription ID
	[string] [Parameter(Mandatory=$true)] $mediastorageaccount, #Media Library Storage Account Name
	[string] [Parameter(Mandatory=$true)] $SharedPath, #PDDS shared path for folder creation
	[string] [Parameter(Mandatory=$true)] $ServicePrincipalName, #For Key Vault Creation
	[string] [Parameter(Mandatory=$true)] $azureAccountName, #Service Account used for provisioning in Azure
    [string] [Parameter(Mandatory=$true)] $AzureAccountPwd, #Service Account Password
    [string] [Parameter(Mandatory=$true)] $subscriptionId, #Subscription ID to which the deployment is done
    [switch] $UploadArtifacts,
    [string] $StorageAccountName,
    [string] $StorageContainerName = $ResourceGroupName.ToLowerInvariant() + '-stageartifacts',
    [string] $TemplateFile = 'Templates/Catalogue Templates/gdapdeploy-catalogue.json',
    [string] $TemplateParametersFile = 'Templates/Catalogue Templates/gdapdeploy-catalogue.parameters.json',
	[string] $ConfigFile = 'Templates/Catalogue Templates/Diagnostic_config.xml',
    [string] $ArtifactStagingDirectory = '.',
    [string] $DSCSourceFolder = 'DSC',
	[string] $NewTemplateParametersFile = 'Templates/Catalogue Templates/new.parameters.json',
    [switch] $ValidateOnly
)

# Clear screen
cls

# Shakeout Flag
$ShakeoutFlag = $true

if($ShakeoutFlag)
{
    Write-Output "Performing Shakeout deployment only..."
}

#Function to login automatically without prompting for login details
Import-Module AzureRM -ErrorAction SilentlyContinue
Import-Module Azure -ErrorAction SilentlyContinue
$azurePassword = ConvertTo-SecureString $AzureAccountPwd -AsPlainText -Force 
$psCred = New-Object System.Management.Automation.PSCredential($azureAccountName, $azurePassword) 

Try{
    Login-AzureRmAccount -Credential $psCred -SubscriptionId $subscriptionId -ErrorAction Stop
    Write-Output "Azure portal login successful"
    } Catch {
	    "An error occurred while completing the request. Couldn't Login to the Azure Portal. Please contact the support team"
	    throw "Error Details for Support: $_.Exception.Message"
        exit
}


#Function to check for an active Azure Session
function Check-Session () {
$ErrorActionPreference = "SilentlyContinue"
    $Error.Clear()

    #if context already exist
    Get-AzureRmContext -ErrorAction Ignore 
    foreach ($eacherror in $Error) {
        if ($eacherror.Exception.ToString() -like "*Run Login-AzureRmAccount to login.*") {
            Login-AzureRmAccount
        }
    }

    $Error.Clear();
}
#check if session exists, if not then prompt for login
Check-Session

##############################################
#Defining Additional Variables for the script#
##############################################


# HOTFIX GDAP-2279
# - Create an updated string which will be used to replace all impacted object names such as
# - Azure Resource Group, Analytics Lab AD Group etc.

$MAX_HOSTNAME_LENGTH = 15 # Maximum length of a host name for error handling

# Only execute if hostname length has been exceeded
if ($vmname.Length -gt 15)
{
    "WARNING: Urban Code hostname has exceeded 15 characters! Please contact UCD support team, hostname: $($vmname)"

    $labID = "$($vmname.Substring(9,7))"
     "LabID passed from UrbanCode: $($labID)"

    $newLabID = "$($labID.Substring(0,3))$($labID.Substring(4,3))"
    "Modified LabID $($newLabID) removing additional digit"

    $vmname = $vmname -replace $labID, $newLabID
    
    $Resourcegroupname = $Resourcegroupname  -replace $labID, $newLabID
    $AnalyticsLabADGroupName = $AnalyticsLabADGroupName  -replace $labID, $newLabID
    $AnalyticsLabAdminADGroupName = $AnalyticsLabAdminADGroupName -replace $labID, $newLabID
    $vmname = $vmname -replace $labID, $newLabID
    $SAname = $SAname -replace $labID, $newLabID
    $SQLname = $SQLname -replace $labID, $newLabID
    $adfname = $adfname -replace $labID, $newLabID

    "New object names:
    $($Resourcegroupname)
    $($AnalyticsLabADGroupName)
    $($AnalyticsLabAdminADGroupName) 
    $($vmname)
    $($SAname)
    $($SQLname)
    $($adfname) "

}

# END OF GDAP-2279 Hotfix




Try{
$subscriptionName = (Get-AzureRmSubscription -SubscriptionId $subscriptionid -ErrorAction Stop -WarningAction SilentlyContinue).SubscriptionName
Write-Output "Initiating deployment in $subscriptionName"
} Catch{
	"An error occurred while completing the request. Issues while fetching the subscription name"
	throw "Error Details for Support: $_.Exception.Message"
    exit 
}


switch ($subscriptionName)
    {
        QBE-NA-NAO-N-GDAP {$timeZone = 'Pacific Standard Time'; $omsdivision = 'NA'; $omsenv = 'NPROD'}
        QBE-AU-ANZO-N-GDAP {$timeZone = 'E. Australia Standard Time'; $omsdivision = 'AO'; $omsenv = 'NPROD'}
        QBE-NA-EM-N-GDAP {$timeZone = 'Pacific Standard Time'; $omsdivision = 'NA'; $omsenv = 'NPROD'}
		QBE-AU-EM-N-GDAP {$timeZone = 'E. Australia Standard Time'; $omsdivision = 'AO'; $omsenv = 'NPROD'}
        QBE-AU-GROUP-N-GDAP {$timeZone = 'E. Australia Standard Time'; $omsdivision = 'AO'; $omsenv = 'NPROD'}
		QBE-GB-EO-N-GDAP {$timeZone = 'GMT Standard Time'; $omsdivision = 'EO'; $omsenv = 'NPROD'}
		QBE-AU-GCC-N-GDAP {$timeZone = 'E. Australia Standard Time'; $omsdivision = 'AO'; $omsenv = 'NPROD'}
        QBE-NA-NAO-P-GDAP {$timeZone = 'Pacific Standard Time'; $omsdivision = 'NA'; $omsenv = 'PROD'}
        QBE-AU-ANZO-P-GDAP {$timeZone = 'E. Australia Standard Time'; $omsdivision = 'AO'; $omsenv = 'PROD'}
        QBE-AU-GCC-P-GDAP {$timeZone = 'E. Australia Standard Time'; $omsdivision = 'AO'; $omsenv = 'PROD'}
        QBE-AU-GROUP-P-GDAP {$timeZone = 'E. Australia Standard Time'; $omsdivision = 'AO'; $omsenv = 'PROD'}
        QBE-NA-EM-P-GDAP {$timeZone = 'Pacific Standard Time'; $omsdivision = 'NA'; $omsenv = 'PROD'}
		QBE-AU-EM-P-GDAP {$timeZone = 'E. Australia Standard Time'; $omsdivision = 'AO'; $omsenv = 'PROD'}
		QBE-GB-EO-P-GDAP {$timeZone = 'GMT Standard Time'; $omsdivision = 'EO'; $omsenv = 'PROD'}
		default {$timeZone = 'E. Australia Standard Time'; $omsdivision = 'AO'; $omsenv = 'NPROD'}
	}
	
Write-Output "The time zone for the Analytics Lab is $timeZone"


#Getting Media Library parameters
Try{ 
    Select-AzureRmSubscription -SubscriptionId $mediasubscriptionid -ErrorAction Stop -WarningAction SilentlyContinue
    Write-Output "Selected Media Library Subscription for fetching Media library parameters"
} Catch {
	"An error occurred while completing the request. Issues while connecting to media library subscription"
	throw "Error Details for Support: $_.Exception.Message"
    exit 
}
$mediacontainer = 'gdap'
$mediastorageacckey = (Get-AzureRmStorageAccountKey -ResourceGroupName $mediarg -AccountName $mediastorageaccount).Value[0]  
$mediasaContext = New-AzureStorageContext  -StorageAccountName $mediastorageaccount -StorageAccountKey $mediastorageacckey  
Try{
    $sastoken = New-AzureStorageContainerSASToken -Container $mediacontainer -Context $mediasaContext -Permission r -ExpiryTime (Get-Date).AddHours(5) -ErrorAction Stop
    Write-Output "SAS token generated for 5 hours.."
} Catch {
	"An error occurred while completing the request. Issues while generating sas-token for media library access"
	throw "Error Details for Support: $_.Exception.Message"
    exit 
}



if($omsenv -eq 'NPROD'){
	$keyvaultnamegis = 'EnvMgmtKeyVault' #The KeyVault name is same for all NonProd Subscriptions
	}else{
	$keyvaultnamegis = 'EnvMgmtKeyVault' + $omsdivision + $omsenv
}


if(!$ShakeoutFlag){


    Write-Output "The Environment Management Key Vault is $keyvaultnamegis"

    $omsserver = 'OMSGateWayServer' + $omsdivision

    Try{
    $EnvMgmtUser = (Get-AzureKeyVaultSecret -VaultName $keyvaultnamegis -Name 'EnvmtMgmtUserName' -ErrorAction Stop).SecretValueText
    Write-Output "The Environment Management User is $EnvMgmtUser"
    $EnvMgmtUserPwd = (Get-AzureKeyVaultSecret -VaultName $keyvaultnamegis -Name 'EnvmtMgmtUserPassword' -ErrorAction Stop).SecretValueText
    $aadClientSecret = (Get-AzureKeyVaultSecret -VaultName $keyvaultnamegis -Name 'AADClientSecret' -ErrorAction Stop).SecretValueText
    $omsgatewayserver = (Get-AzureKeyVaultSecret -VaultName $keyvaultnamegis -Name $omsserver -ErrorAction Stop).SecretValueText
    Write-Output "The OMS GateWay Server is $omsgatewayserver"
    $omsworkspaceid = (Get-AzureKeyVaultSecret -VaultName $keyvaultnamegis -Name 'OMSWorkSpaceID' -ErrorAction Stop).SecretValueText
    $omsworkspacekey = (Get-AzureKeyVaultSecret -VaultName $keyvaultnamegis -Name 'OMSWorkSpaceKey' -ErrorAction Stop).SecretValueText
    } Catch {
	    "An error occurred while completing the request. Failed to fetch details from GIS KeyVault"
	    throw "Error Details for Support: $_.Exception.Message"
        exit 
    }
}


#Source Storage Account of VHD
$srcUri = $sharedtemplateurl + 'qbevhdstore/qbevhd03.vhd'
Write-Output "The qbe image repository is $srcUri"
$project_owner = $LANID
$analyticslabid = $ResourceGroupName.substring($ResourceGroupName.Length-6, 6)

#Defining Media Library URLs
$dscurl = $sharedtemplateurl + 'dscscripts/InstallAll.zip'
$sharedtemplateurlvm = $sharedtemplateurl + 'ARMTemplates/SharedTemplates/gdapdeploy-wvm-sit.json'
$sharedtemplateurlsa = $sharedtemplateurl + 'ARMTemplates/SharedTemplates/gdapdeploy-storageaccount-sit.json'
$sharedtemplateurlsql = $sharedtemplateurl + 'ARMTemplates/SharedTemplates/gdapdeploy-dw.json'
$sharedtemplateurladf =  $sharedtemplateurl + 'ARMTemplates/SharedTemplates/gdapdeploy-adf.json'


#Randomly generating Admin Credentials for DevLab
$adminUsername = 'qbeadmin'
$randomString = '0123456789abcdefghijklmnopqrstuvwxyz'.ToCharArray()
$adminPassword = 'Password@1'+(-join ($randomString | Get-Random -Count 5 | % {[char]$_}))

#Deriving PDDS Computer name from SharePath parameter
$computername = $SharedPath.Substring(2,15) + '.' + $domaintojoin
#Deriving project path in PDDS server
$ProjectPath = "$Sharedpath\Project"
$ProjectPathDD = "$Sharedpath\Data_Download\Project"
$ProjectPathDU = "$Sharedpath\Data_Upload\Project"

#Select the required Azure Subscription
Try{ 
    Select-AzureRmSubscription -SubscriptionId $subscriptionId -ErrorAction Stop
    Write-Output "Selected the Subscription for provisioning"
} Catch {
	"An error occurred while completing the request. Issues while connecting to subscription"
	throw "Error Details for Support: $_.Exception.Message"
    exit 
}


#Validations prior to execution
#PDDS Project folder validation

if(!$ShakeoutFlag){

    function TestPddsPath($pddspath)
    {
	    return $(Test-Path "$pddspath")
    }

    function verifypath($sharepath)
    {
	    $pdd=$sharepath
	    $computernamepdds=$SharedPath.split("\")[2]
	    $ret_status=Invoke-Command -Credential $psCred -ComputerName $computernamepdds -ScriptBlock ${function:TestPddsPath} -ArgumentList $pdd  -Verbose -ErrorAction Stop
	    return $ret_status
    }

    if( $(verifypath("$ProjectPath\$project_name")) -ne  "True"){
	    "Project folder validation succeeded"
	    } else{
	    "An error occurred while completing the request."
        throw "Error Details for Support: PDDS validation failed. Project Folder Already Exists. Please re-enter the Project Name and Try Again"
	    exit
	    }
	
    if( $(verifypath("$ProjectPathDD\$project_name")) -ne  "True"){
	    "Project folder validation succeeded for Data Download Directory"
	    } else{
	    "An error occurred while completing the request"
	    throw "Error Details for Support: PDDS validation failed. Project Folder Already Exists in Data Download Directory. Please re-enter the Project Name and Try Again"
	    exit
	    }
    if( $(verifypath("$ProjectPathDU\$project_name")) -ne  "True"){
        "Project folder validation succeeded for Data Upload Directory"
    } 
    else{
        "An error occurred while completing the request"
        throw "Error Details for Support: PDDS validation failed. Project Folder Already Exists in Data Upload Directory. Please re-enter the Project Name and Try Again"
    
        if(!$ShakeoutFlag){
            exit
        }    
    }
}


#Azure Resource group validation
if((Get-AzureRmResourceGroup | Select-Object ResourceGroupName | Where-Object {$_.ResourceGroupName -eq $ResourceGroupName}).ResourceGroupName -eq $ResourceGroupName){

	"An error occurred while completing the request."
	throw "Error Details for Support: Resource group with the same name already exists"

    if(!$ShakeoutFlag){
        exit
    }
	

	} else{
	    "Resource Group Validation succeeded"
	}

	
#Defining parameters for Active Directory
$SrvUsrPassword = ConvertTo-SecureString $domainpassword -AsPlainText -Force
$Mycred = New-Object System.Management.Automation.PSCredential ($domainuser, $SrvUsrPassword)


#ADGroup Validation Corp Domain
Try{
(Get-ADGroup -Identity $AnalyticsLabADGroupName -Server $domaintojoin -Credential $Mycred).Name
} Catch{
     if ($_.Exception.Message -like "Cannot find an object with identity:*"){
         "AD Group Validation succeeded in Global Domain"
     } else{
        "An error occurred while completing the request. Error accessing QBE Active Directory. Possibility of an AD account lock out."
        throw "Error Details for Support: $_.Exception.Message"
        
        if(!$ShakeoutFlag){
            exit
        }
        
     }
}


#LANID Validation
Try{
$user = (Get-ADUser $LANID -Server $userdomain -ErrorAction Stop).Name
} Catch{
     if ($_.Exception.Message -like "Cannot find an object with identity:*"){
         "An error occurred while completing the request."
		 throw "Error Details for Support: Invalid LANID"
        if(!$ShakeoutFlag){
            exit
        }
         } else{
             
            "An error occurred while completing the request. Error accessing QBE Regional Active Directory. Possibility of an AD account lock out."
            throw "Error Details for Support: $_.Exception.Message"

            if(!$ShakeoutFlag){
                exit
            }		 
        }
}
#########################VALIDATIONS COMPLETED###################################


#Create Dev-Lab specific Active Directory groups in Corp Domain
Try{
    New-ADGroup -Name $AnalyticsLabADGroupName -GroupScope DomainLocal -Path $groupoupath -Server $domaintojoin -Credential $Mycred -ErrorAction Stop
	"$AnalyticsLabADGroupName created in Global domain for the Analytics Lab $analyticslabid"
	New-ADGroup -Name $AnalyticsLabAdminADGroupName -GroupScope DomainLocal -Path $groupoupath -Server $domaintojoin -Credential $Mycred -ErrorAction Stop
	"$AnalyticsLabAdminADGroupName created in Global domain for the Analytics Lab $analyticslabid"
} Catch {
	"An error occurred while completing the request. AD Group creation in Corp Domain failed.."
	throw "Error Details for Support: $_.Exception.Message"
    exit 
}

#Assigning Owner ID to add to the groups
Try{
    $CurrentUser = Get-ADUser $LANID -Server "$userdomain" -ErrorAction Stop
	$CurrentUserCorp = Get-ADUser $LANID -Server "$domainToJoin" -ErrorAction Stop
} Catch {

	"An error occurred while completing the request."
	throw "Error Details for Support: $_.Exception.Message"
    
    if(!$ShakeoutFlag){
    	Remove-ADGroup -Identity $AnalyticsLabADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
	    Remove-ADGroup -Identity $AnalyticsLabAdminADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
        exit
    }
     
}

#User added to Analytics Lab AD Group
Try{ 
    Add-ADGroupMember -Identity $AnalyticsLabADGroupName -Member $CurrentUser -Server $domaintojoin -Credential $Mycred -ErrorAction Stop
    "Regional User $LANID added to $AnalyticsLabADGroupName"
	Add-ADGroupMember -Identity $AnalyticsLabADGroupName -Member $CurrentUserCorp -Server $domaintojoin -Credential $Mycred -ErrorAction Stop
	"Global User $LANID added to $AnalyticsLabADGroupName"
} Catch {

	"An error occurred while completing the request."
    throw "Error Details for Support: $_.Exception.Message"

    if(!$ShakeoutFlag){
        Remove-ADGroup -Identity $AnalyticsLabADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
	    Remove-ADGroup -Identity $AnalyticsLabAdminADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
        exit
    }
     
}

#Property added for Azure AD synchronization
$G = Get-ADGroup -Identity $AnalyticsLabADGroupName -Server $domaintojoin
Try{
    Set-ADGroup -Identity $AnalyticsLabADGroupName -Server $domaintojoin -Add @{extensionAttribute12=[System.Convert]::ToBase64String($G.ObjectGUID.ToByteArray())} -Credential $Mycred
    Write-Output "EA12 updated. This property is used for Azure Active Directory Sync"
}Catch {
	
	"An error occurred while completing the request."
	throw "Error Details for Support: $_.Exception.Message"


    if(!$ShakeoutFlag){

        Remove-ADGroup -Identity $AnalyticsLabADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
	    Remove-ADGroup -Identity $AnalyticsLabAdminADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
        exit

    }
}

#Owner tagged to the Development Lab
Try{
Set-ADGroup -Identity $AnalyticsLabADGroupName -Server $domaintojoin -Add @{description = "$LANID"} -Credential $Mycred
"$LANID tagged as Owner tagged to the group $AnalyticsLabADGroupName"
}Catch {

	"An error occurred while completing the request."
	throw "Error Details for Support: $_.Exception.Message"
    if(!ShakeoutFlag){

    	Remove-ADGroup -Identity $AnalyticsLabADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
	    Remove-ADGroup -Identity $AnalyticsLabAdminADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
        exit

    }
 
}



"Starting Analytics Lab Provisioning"

#Creating new Resource Group for the LAB
Try{ 
	New-AzureRmResourceGroup -Name $ResourceGroupName -Location $ResourceGroupLocation -Verbose -Force -ErrorAction Stop
    Write-Output "Resource Group $ResourceGroupName created in $ResourceGroupLocation"
} Catch {
	
	"An error occurred while completing the request. Resource group creation failed"
	throw "Error Details for Support: $_.Exception.Message"

    if(!$ShakeoutFlag){
        Remove-ADGroup -Identity $AnalyticsLabADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
	    Remove-ADGroup -Identity $AnalyticsLabAdminADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
        exit 
    }
}


#The following code is to copy  the VHD from a source location for the VM which we are going to Provision
$StorageAccountName = 'gldyndapzpsavhd'+$analyticsLabid
Write-Output "The storage account of the analytics lab VM is $StorageAccountName"
$Location = (Get-AzureRmResourceGroup -Name $ResourceGroupName).Location

$templateurivhdsa = $sharedtemplateurlsa+$sastoken

Try{
    New-AzureRmResourceGroupDeployment -Name $StorageAccountName -ResourceGroupName $ResourceGroupName -TemplateUri $templateurivhdsa -project_name $project_name -project_id $project_id -environment $environment -category_role $category_role -division $division -project_owner $project_owner -pii_use $pii_use -data_source_name $data_source_name -business_unit $business_unit -contact_email $contact_email -expiration_date $expiration_date -cost_center $cost_center -storageaccountName $StorageAccountName -ErrorAction Stop
    Write-Output "$StorageAccountName created for Analytics Lab VM"
} Catch {

	"An error occurred while completing the request."
	throw "Error Details for Support: $_.Exception.Message"

    if(!$ShakeoutFlag){
	    Remove-ADGroup -Identity $AnalyticsLabADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
	    Remove-ADGroup -Identity $AnalyticsLabAdminADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
        Remove-AzureRmResourceGroup -Name $ResourceGroupName -Force
        exit
    }
}

#The below snippet is for copying QBE VHD for Windows Provisioning
#Target Storage Account
$key1 = (Get-AzureRmStorageAccountKey -ResourceGroupName $ResourceGroupName -AccountName $StorageAccountName).Value[0]  
#Create the destination storage account context
$StorageAccountContext = New-AzureStorageContext  –StorageAccountName $StorageAccountName `
                                        -StorageAccountKey $key1  
#Destination Container Name
$containerName = "vmvhds"

Try{ 
   New-AzureStorageContainer -Name $containerName -Context $StorageAccountContext -ErrorAction Stop
   Write-Output "Container $containerName created for VHD storage"
} Catch {

	"An error occurred while completing the request."
	throw "Error Details for Support: $_.Exception.Message"

    if(!$ShakeoutFlag){
	    Remove-ADGroup -Identity $AnalyticsLabADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
	    Remove-ADGroup -Identity $AnalyticsLabAdminADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
        Remove-AzureRmResourceGroup -Name $ResourceGroupName -Force
        exit 
    }

}

Try{ 
   #Start the asynchronous copy - specify the source authentication with -SrcContext
    $blob1 = Start-AzureStorageBlobCopy -srcUri $srcUri `
                                        -SrcContext $mediasaContext `
                                        -DestContainer $containerName `
                                        -DestBlob "qbevhd.vhd" `
                                        -DestContext $StorageAccountContext `
                                        -ErrorAction Stop
                                        #Copy of VHD ends over here
} Catch {

	"An error occurred while completing the request."
	throw "Error Details for Support: $_.Exception.Message"

    if(!$ShakeoutFlag){

	    Remove-ADGroup -Identity $AnalyticsLabADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
	    Remove-ADGroup -Identity $AnalyticsLabAdminADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
        Remove-AzureRmResourceGroup -Name $ResourceGroupName -Force

        exit 

    }

}

#Check for vhd copy success
$copywait = 0
do
{
    $Copystatus = (Get-AzureStorageBlobCopyState -Blob "qbevhd.vhd" -Container $containerName -Context $StorageAccountContext).Status
    $copywait ++
    sleep 60
}
while (($Copystatus -ne "Success") -and ($copywait -le 30))


if ($copywait -gt 30){

    if($ShakeoutFlag){
    	Remove-ADGroup -Identity $AnalyticsLabADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
	    Remove-ADGroup -Identity $AnalyticsLabAdminADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
        Remove-AzureRmResourceGroup -Name $ResourceGroupName -Force    
    }

    Write-Output "An error occurred while completing the request."
    throw "Error Details for Support: Unusual time to copy QBE Image. Please contact the support team"
    exit

} else{
    Write-Output "QBE Image copied for Windows VM provisioning"
}



#Function to write inputs from PowerShell into the parameter file for ARM Template execution
function setParamName ()
{
    $JsonContent = Get-Content $TemplateParametersFile -Raw | ConvertFrom-Json
    $JsonParameters = $JsonContent | Get-Member -Type NoteProperty | Where-Object {$_.Name -eq "parameters"}
    $JsonContent.parameters[0].project_name.value = $project_name
    $JsonContent.parameters[0].project_id.value = $project_id
    $JsonContent.parameters[0].environment.value = $environment
    $JsonContent.parameters[0].category_role.value = $category_role
    $JsonContent.parameters[0].division.value = $division
    $JsonContent.parameters[0].project_owner.value = $project_owner
    $JsonContent.parameters[0].pii_use.value = $pii_use
    $JsonContent.parameters[0].data_source_name.value = $data_source_name
    $JsonContent.parameters[0].business_unit.value = $business_unit
    $JsonContent.parameters[0].contact_email.value = $contact_email
    $JsonContent.parameters[0].expiration_date.value = $expiration_date
	$JsonContent.parameters[0].cost_center.value = $cost_center
    $JsonContent.parameters[0].cataloguesize.value = $cataloguetype
    $JsonContent.parameters[0].vnetResourceGroup.value = $vnetrg
    $JsonContent.parameters[0].vnetname.value = $vnetname 
    $JsonContent.parameters[0].subnetname.value = $Subnetname
    $JsonContent.parameters[0].vmname.value = $vmname
	$JsonContent.parameters[0].adfname.value = $adfname
    $JsonContent.parameters[0].vmsa.value = $StorageAccountName #The storage account of Windows VM(VHD is copied)
    $JsonContent.parameters[0].dscurl.value = $dscurl
    $JsonContent.parameters[0].domainToJoin.value = $domaintojoin
    $JsonContent.parameters[0].ouPath.value =$oupath
    $JsonContent.parameters[0].domainJoinOptions.value =$domainjoinoptions
    $JsonContent.parameters[0].domainUsername.value = $domainuser
    $JsonContent.parameters[0].domainPassword.value = $domainpassword  
    $JsonContent.parameters[0].storageaccountName.value = $SAname
    $JsonContent.parameters[0].sqlservername.value = $SQLname
    $JsonContent.parameters[0].qbesqldwName.value = $dwname
    $JsonContent.parameters[0].sharedtemplateurlwvm.value = $sharedtemplateurlvm
    $JsonContent.parameters[0].sharedtemplateurlsa.value = $sharedtemplateurlsa
    $JsonContent.parameters[0].sharedtemplateurlsql.value = $sharedtemplateurlsql
	$JsonContent.parameters[0].sharedtemplateurladf.value = $sharedtemplateurladf
    $JsonContent.parameters[0].sastoken.value = $sastoken
    $JsonContent.parameters[0].ADGroupname1.value = $AnalyticsLabAdminADGroupName
	$JsonContent.parameters[0].ADGroupname2.value = $AnalyticsLabADGroupName
	$JsonContent.parameters[0].admingroup1.value = $ITServerAdminsGroupName
	$JsonContent.parameters[0].admingroup2.value = $ServerOperationsToolsGroupName
	$JsonContent.parameters[0].admingroup3.value = $ServerEngineersNonProdGroupName
	$JsonContent.parameters[0].scomserver.value = $scomserver
	$JsonContent.parameters[0].dscstorageacckey.value = $mediastorageacckey
	$JsonContent.parameters[0].dsccontainer.value = $mediacontainer
	$JsonContent.parameters[0].dscstorageaccount.value = $mediastorageaccount
	$JsonContent.parameters[0].analyticsLabid.value = $analyticslabid
    $JsonContent.parameters[0].SharedPath.value = $SharedPath
    $JsonContent.parameters[0].adminUsername.value = $adminUsername
    $JsonContent.parameters[0].adminPassword.value = $adminPassword
	$JsonContent.parameters[0].timeZone.value = $timeZone
    $JsonContent | ConvertTo-Json -depth 999 |  Out-File  -Encoding default  -FilePath $NewTemplateParametersFile   
}


Import-Module Azure -ErrorAction SilentlyContinue

try {
    [Microsoft.Azure.Common.Authentication.AzureSession]::ClientFactory.AddUserAgent("VSAzureTools-$UI$($host.name)".replace(" ","_"), "2.9.5")
} catch { }

Set-StrictMode -Version 3

function Format-ValidationOutput {
    param ($ValidationOutput, [int] $Depth = 0)
    Set-StrictMode -Off
    return @($ValidationOutput | Where-Object { $_ -ne $null } | ForEach-Object { @("  " * $Depth + $_.Code + ": " + $_.Message) + @(Format-ValidationOutput @($_.Details) ($Depth + 1)) })
}

$OptionalParameters = New-Object -TypeName Hashtable
$TemplateFile = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, $TemplateFile))
$TemplateParametersFile = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, $TemplateParametersFile))
$NewTemplateParametersFile = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, $NewTemplateParametersFile))
$TemplateFileVariables = Get-Content $TemplateFile -Raw | ConvertFrom-Json


setParamName 
$NewTemplateFileVariables = Get-Content $NewTemplateParametersFile -Raw | ConvertFrom-Json


if ($UploadArtifacts) {
    # Convert relative paths to absolute paths if needed
    $ArtifactStagingDirectory = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, $ArtifactStagingDirectory))
    $DSCSourceFolder = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, $DSCSourceFolder))

    Set-Variable ArtifactsLocationName '_artifactsLocation' -Option ReadOnly -Force
    Set-Variable ArtifactsLocationSasTokenName '_artifactsLocationSasToken' -Option ReadOnly -Force

    $OptionalParameters.Add($ArtifactsLocationName, $null)
    $OptionalParameters.Add($ArtifactsLocationSasTokenName, $null)

    # Parse the parameter file and update the values of artifacts location and artifacts location SAS token if they are present
    $JsonContent = Get-Content $TemplateParametersFile -Raw | ConvertFrom-Json
    $JsonParameters = $JsonContent | Get-Member -Type NoteProperty | Where-Object {$_.Name -eq "parameters"}

    if ($JsonParameters -eq $null) {
        $JsonParameters = $JsonContent
    }
    else {
        $JsonParameters = $JsonContent.parameters
    }

    $JsonParameters | Get-Member -Type NoteProperty | ForEach-Object {
        $ParameterValue = $JsonParameters | Select-Object -ExpandProperty $_.Name

        if ($_.Name -eq $ArtifactsLocationName -or $_.Name -eq $ArtifactsLocationSasTokenName) {
            $OptionalParameters[$_.Name] = $ParameterValue.value
        }
    }

    # Create DSC configuration archive
    if (Test-Path $DSCSourceFolder) {
        $DSCSourceFilePaths = @(Get-ChildItem $DSCSourceFolder -File -Filter "*.ps1" | ForEach-Object -Process {$_.FullName})
        foreach ($DSCSourceFilePath in $DSCSourceFilePaths) {
            $DSCArchiveFilePath = $DSCSourceFilePath.Substring(0, $DSCSourceFilePath.Length - 4) + ".zip"
            Publish-AzureRmVMDscConfiguration $DSCSourceFilePath -OutputArchivePath $DSCArchiveFilePath -Force -Verbose
        }
    }

    # Create a storage account name if none was provided
    if($StorageAccountName -eq "") {
        $subscriptionId = ((Get-AzureRmContext).Subscription.SubscriptionId).Replace('-', '').substring(0, 19)
        $StorageAccountName = "stage$subscriptionId"
    }

    $StorageAccount = (Get-AzureRmStorageAccount | Where-Object{$_.StorageAccountName -eq $StorageAccountName})

    # Create the storage account if it doesn't already exist
    if($StorageAccount -eq $null){
        $StorageResourceGroupName = "ARM_Deploy_Staging"
        New-AzureRmResourceGroup -Location "$ResourceGroupLocation" -Name $StorageResourceGroupName -Force
        
        Try{
           $StorageAccount = New-AzureRmStorageAccount -StorageAccountName $StorageAccountName -Type 'Standard_LRS' -ResourceGroupName $StorageResourceGroupName -Location "$ResourceGroupLocation" -ErrorAction Stop
        } Catch {
            
            "An error occurred while completing the request."
            throw "Error Details for Support: $_.Exception.Message"
            
            # Only exit if the shakeout flag is not set
            if(!$ShakeoutFlag)
            {
                Remove-ADGroup -Identity $AnalyticsLabADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
			    Remove-ADGroup -Identity $AnalyticsLabAdminADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
                Remove-AzureRmResourceGroup -Name $ResourceGroupName -Force	
                exit
            } 
        }
        
    }

    $StorageAccountContext = (Get-AzureRmStorageAccount | Where-Object{$_.StorageAccountName -eq $StorageAccountName}).Context

    # Generate the value for artifacts location if it is not provided in the parameter file
    $ArtifactsLocation = $OptionalParameters[$ArtifactsLocationName]
    if ($ArtifactsLocation -eq $null) {
        $ArtifactsLocation = $StorageAccountContext.BlobEndPoint + $StorageContainerName
        $OptionalParameters[$ArtifactsLocationName] = $ArtifactsLocation
    }

    # Copy files from the local storage staging location to the storage account container
    New-AzureStorageContainer -Name $StorageContainerName -Context $StorageAccountContext -Permission Container -ErrorAction SilentlyContinue *>&1

    $ArtifactFilePaths = Get-ChildItem $ArtifactStagingDirectory -Recurse -File | ForEach-Object -Process {$_.FullName}
    foreach ($SourcePath in $ArtifactFilePaths) {
        $BlobName = $SourcePath.Substring($ArtifactStagingDirectory.length + 1)
        Set-AzureStorageBlobContent -File $SourcePath -Blob $BlobName -Container $StorageContainerName -Context $StorageAccountContext -Force -ErrorAction Stop
    }

    # Generate the value for artifacts location SAS token if it is not provided in the parameter file
    $ArtifactsLocationSasToken = $OptionalParameters[$ArtifactsLocationSasTokenName]
    if ($ArtifactsLocationSasToken -eq $null) {
        # Create a SAS token for the storage container - this gives temporary read-only access to the container
        $ArtifactsLocationSasToken = New-AzureStorageContainerSASToken -Container $StorageContainerName -Context $StorageAccountContext -Permission r -ExpiryTime (Get-Date).AddHours(4)
        $ArtifactsLocationSasToken = ConvertTo-SecureString $ArtifactsLocationSasToken -AsPlainText -Force
        $OptionalParameters[$ArtifactsLocationSasTokenName] = $ArtifactsLocationSasToken
    }
}

    # Create or update the resource group using the specified template file and template parameters file

$ErrorMessages = @()
if ($ValidateOnly) {
    $ErrorMessages = Format-ValidationOutput (Test-AzureRmResourceGroupDeployment -ResourceGroupName $ResourceGroupName `
                                                                                  -TemplateFile $TemplateFile `
                                                                                  -TemplateParameterFile $TemplateParametersFile `
                                                                                  @OptionalParameters `
                                                                                  -Verbose)
}
else {        
        Try{
            New-AzureRmResourceGroupDeployment -Name ((Get-ChildItem $TemplateFile).BaseName + '-' + ((Get-Date).ToUniversalTime()).ToString('MMdd-HHmm')) `
                                    -ResourceGroupName $ResourceGroupName `
                                    -TemplateFile $TemplateFile `
                                    -TemplateParameterFile $NewTemplateParametersFile `
                                    @OptionalParameters `
                                    -Force -Verbose `
                                    -ErrorVariable ErrorMessages `
                                    -ErrorAction Stop
        } Catch {
			
            if(!$ShakeoutFlag){
                Remove-ADGroup -Identity $AnalyticsLabADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
			    Remove-ADGroup -Identity $AnalyticsLabAdminADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
                Remove-AzureRmResourceGroup -Name $ResourceGroupName -Force					
            }
            
            "An error occurred while completing the request. Rolling back analytics Lab if ShakeoutFlag is false. Flag: ${ShakeoutFlag}"
			throw "Error Details for Support: $_.Exception.Message"
            exit 
        }
		
    $ErrorMessages = $ErrorMessages | ForEach-Object { $_.Exception.Message.TrimEnd("`r`n") }
}
if ($ErrorMessages)
{
    "", ("{0} returned the following errors:" -f ("Template deployment", "Validation")[[bool]$ValidateOnly]), @($ErrorMessages) | ForEach-Object { Write-Output $_ }
}

###############################
## Updated by Tony George
## Defect: GDAP-1792
## Description: Insufficient Logging (Azure Portal) - Portal (Logging)
###############################
$ConfigFile = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, $ConfigFile))
$diagsa = 'gldyndapzpsadiag'+$analyticslabid

Try{
Set-AzureRmVMDiagnosticsExtension -ResourceGroupName $ResourceGroupName -VMName $vmname -DiagnosticsConfigurationPath $ConfigFile -StorageAccountName $diagsa -ErrorAction Stop -WarningAction SilentlyContinue
} Catch{
		"Warning Message: Issue with Portal logging"
		"Error Details for Support: $_.Exception.Message"
		"Message to End User: Please contact the support team for a manual enablement of logging"
}

"Starting VM Backup"
Try{
Get-AzureRmRecoveryServicesVault -Name $existingRecoveryServicesVaultName -ErrorAction Stop | Set-AzureRmRecoveryServicesVaultContext -ErrorAction Stop
	} Catch {
		"Warning Message: Issue with VM Backup"
		"Error Details for Support: $_.Exception.Message"
		"Message to End User: Please contact the support team for a manual back-up"
}
Try{
$pol=Get-AzureRmRecoveryServicesBackupProtectionPolicy -Name $existingBackupPolicyName -ErrorAction Stop
	} Catch {
		"Warning Message: Issue with VM Backup"
		"Error Details for Support: $_.Exception.Message"
		"Message to End User: Please contact the support team for a manual back-up"
}
Try{
Enable-AzureRmRecoveryServicesBackupProtection -Policy $pol -Name $vmname -ResourceGroupName $ResourceGroupName -ErrorAction Stop
"Completed VM Backup"
	} Catch {
		"Warning Message: Issue with VM Backup"
		"Error Details for Support: $_.Exception.Message"
		"Message to End User: Please contact the support team for a manual back-up"
}


#OMS Integration of Windows VM
Write-Output "Starting OMS Integration for Windows"

$ExtensionName = 'MicrosoftMonitoringAgent'
$Publisher = 'Microsoft.EnterpriseCloud.Monitoring'
$Version = '1.0'
$PublicConf = '{
    "workspaceId": "'+$omsworkspaceid+'",
    "stopOnMultipleConnections": false
}'
$PrivateConf = '{
    "workspaceKey": "'+$omsworkspacekey+'",
    "proxy": "'+$omsgatewayserver+'",
    "vmResourceId": "/subscriptions/$subscriptionid/resourceGroups/$ResourceGroupName/providers/Microsoft.Compute/virtualMachines/$vmname"
}'


Try{
Set-AzureRmVMExtension -ResourceGroupName $ResourceGroupName -VMName $vmname -Location $ResourceGroupLocation -Name $ExtensionName -Publisher $Publisher -ExtensionType $ExtensionName -TypeHandlerVersion $Version -Settingstring $PublicConf -ProtectedSettingString $PrivateConf -ErrorAction Stop
} Catch{
		"Warning Message: Issue with OMS integration"
		"Error Details for Support: $_.Exception.Message"
		"Message to End User: Please contact the support team"
}

#Checking Azure AD sync for PaaS authentication
$i = 1
if((Get-AzureRmADGroup -SearchString $AnalyticsLabADGroupName) -eq $null){
	do{
	"Waiting for Analytics Lab AD group to sync to Azure Active Directory...."
	$AzureADStatus = Get-AzureRmADGroup -SearchString $AnalyticsLabADGroupName
	$i++
	sleep 60
	}
	while(($i -le 10) -and ($AzureADStatus -eq $null))
	} else {
	$aadsync = "Yes"
}


if(Get-AzureRmADGroup -SearchString $AnalyticsLabADGroupName){
$aadsync = "Yes"
Write-Output "Analytics Lab AD Group $AnalyticsLabADGroupName synced with Azure Active Directory"
} else {
$aadsync = "No"
Write-Output "Analytics Lab AD Group $AnalyticsLabADGroupName didn't sync with Azure Active Directory. Please contact the support team for manual execution of AAD related tasks"
}

if($aadsync -eq "Yes"){
	Try{
	Set-AzureRmSqlServerActiveDirectoryAdministrator -ResourceGroupName $ResourceGroupName -ServerName $SQLname -DisplayName $AnalyticsLabADGroupName -ErrorAction Stop
	"Active Directory Admin Set for SQL Server"
	} Catch {
	"Warning Message: Paas Authentication Failed"
	"Message to End User: Please contact the support team to set SQL server Active Directory Administrator"
	"Message to End User: The Active Directory group name is $AnalyticsLabADGroupName"
	}
} else{
	"Warning Message: Paas Authentication Failed"
	"Message to End User: Please contact the support team to set SQL server Active Directory Administrator"
	"Message to End User: The Active Directory group name is $AnalyticsLabADGroupName"
}

#Enable Auditing in SQL Server
$sqldiagsa = 'gldyndapzpsadiag'+$analyticsLabid
Try{
Set-AzureRmSqlServerAuditingPolicy -ResourceGroupName $ResourceGroupName -ServerName $SQLname -AuditType Blob -StorageAccountName $sqldiagsa -ErrorAction Stop -WarningAction SilentlyContinue
} Catch{
	"Warning Message: Enabling auditing for SQL Server failed"
	"Message to End User: Please contact the support team to set SQL server Auditing Policy"
	"Message to End User: $_.Exception.Message"
}

Import-Module AzureRM -ErrorAction SilentlyContinue
Import-Module Azure -ErrorAction SilentlyContinue
Try{
	$EnvMgmtUserPwdS = ConvertTo-SecureString $EnvMgmtUserPwd -AsPlainText -Force 
	$EnvMgmtUserCred = New-Object System.Management.Automation.PSCredential($EnvMgmtUser, $EnvMgmtUserPwdS)
	} Catch {
		"An error occurred while completing the request. Environment Management Credentials not stored in KeyVault"
	    throw "Error Details for Support: $_.Exception.Message"
	}
Try{
    Login-AzureRmAccount -Credential $EnvMgmtUserCred -SubscriptionId $subscriptionId -ErrorAction Stop
    Write-Output "Logged in to Azure as $EnvMgmtUser"
    } Catch {
	    "An error occurred while completing the request. Couldn't Login to Azure Portal using EnvMgmt User"
	    throw "Error Details for Support: $_.Exception.Message"
        exit
}	

#Key Vault Integration
$subscriptionName = (Get-AzureRmSubscription -SubscriptionId $subscriptionid -WarningAction SilentlyContinue).SubscriptionName
$keyvaultname = $subscriptionName + 'vault'
Write-Output "The key vault $keyvaultname is used for storing Analytics Lab Secrets"
$VMEncryptionKey = $keyvaultname + 'key'
$KeyVaultResourceGroupName = (Get-AzureRmKeyVault | Where-Object {$_.VaultName -like $keyvaultname}).ResourceGroupName

#Store secrets
Try {
$labadminpassvalue = ConvertTo-SecureString $adminPassword -AsPlainText -Force
Set-AzureKeyVaultSecret -VaultName $keyvaultname  -Name $analyticslabid -SecretValue $labadminpassvalue -ErrorAction Stop
Write-Output "Analytics Lab Admin Credentials stored in the Key Vault : $keyvaultname"
} Catch{
			Remove-ADGroup -Identity $AnalyticsLabADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
			Remove-ADGroup -Identity $AnalyticsLabAdminADGroupName -Server $domaintojoin -Credential $Mycred -Confirm:$false
            Remove-AzureRmResourceGroup -Name $ResourceGroupName -Force			
			"An error occurred while completing the request. Failed to store credentials to Key Vault. Rolling back analytics Lab"
			throw "Error Details for Support: $_.Exception.Message"
            exit 
}

#Encrpyt VM Data Disk
$appDisplayName = (Get-AzureRmADApplication -ApplicationId $ServicePrincipalName).DisplayName
#Secret is fixed
$DEKeyVaultUrl = (Get-AzureRmKeyVault -VaultName $keyvaultname).VaultUri
$KEKURL = (Get-AzureKeyVaultKey -VaultName $keyvaultname -Name $VMEncryptionKey).key.kid
$KEKVRid = (Get-AzureRmKeyVault -VaultName $keyvaultname -ResourceGroupName $KeyVaultResourceGroupName).ResourceId
$Name = 'AzureVMDiskEncryption'

#Encrypting a windows VM
Try{
Set-AzureRmVMDiskEncryptionExtension -ResourceGroupName $ResourceGroupName -VMName $vmname -AadClientID $ServicePrincipalName -AadClientSecret $aadClientSecret -DiskEncryptionKeyVaultUrl $DEKeyVaultUrl -Name $Name -DiskEncryptionKeyVaultId $KEKVRid -KeyEncryptionKeyUrl $KEKURL -KeyEncryptionKeyVaultId $KEKVRid -VolumeType All -Force -ErrorAction Stop
Write-Output "Analytics Lab VM $vmname encrypted"
}
Catch {
	"Warning Message: Issues in VM encryption"
	"Error Details for Support: $_.Exception.Message"
	"Message to End User: Please contact the support team for manual encryption"
}


Try {
Get-AzureStorageBlob -Container $containerName -Context $StorageAccountContext -Blob "qbevhd.vhd" | Remove-AzureStorageBlob -ErrorAction Stop
Write-Output "QBE Image deleted"
} Catch {
"Warning Message: Issues in removing additional VHD"
"Error Details for Support: $_.Exception.Message"
"Message to End User: Please contact the support team to manually delete the copied VHD"
}


#RBAC role assignment. Using Environment Management credentials temporarily since UC service account does not have the required privileges for RBAC role assignment.
if($aadsync -eq "Yes"){
	$ObjectID = (Get-AzureRmADGroup -SearchString $AnalyticsLabADGroupName).id
	New-AzureRmRoleAssignment -RoleDefinitionName "QBE GDAP Analytics Lab User" -Scope "/subscriptions/$subscriptionid/ResourceGroups/$ResourceGroupName" -ObjectId $ObjectID
    "RBAC assigned to $ResourceGroupName"
} else {
"Skipping RBAC"
}

#Reverting back to UC Service Account
Import-Module AzureRM -ErrorAction SilentlyContinue
Import-Module Azure -ErrorAction SilentlyContinue
$azurePassword = ConvertTo-SecureString $AzureAccountPwd -AsPlainText -Force 
$psCred = New-Object System.Management.Automation.PSCredential($azureAccountName, $azurePassword) 

Try{
    Login-AzureRmAccount -Credential $psCred -SubscriptionId $subscriptionId -ErrorAction Stop
    Write-Output "Logged in back to Azure using the account $azureAccountName"
    } Catch {
	    "An error occurred while completing the request. Couldn't Login to Azure Portal"
	    throw "Error Details for Support: $_.Exception.Message"
        exit
}

#Persistent Divisional Data Store Folder creation and access restriction
#Create Project folder in PDDS and give required permissions

function create_project_directory($path, $usernames, $LabUserGroup, $project_id, $Domain)
{
	#Personal
	$Sharepath = "$path\Project"
    Try{
	New-Item -path $Sharepath -Name $project_id -Type directory -ErrorAction Stop
    } Catch{
    "Warning Message: Issues with PDDS folder creation"
    "Error Details for Support: $_.Exception.Message"
    "Message to End User: Please contact your support team raising PDDS folder creation issues.."
    exit
    }
	if($(Test-Path "$Sharepath\$project_id") -eq "True"){
	"Project folder has been created in PDDS Server"
	}
    Try{
	Get-item "$Sharepath\$project_id" -ErrorAction Stop | Disable-NTFSAccessInheritance
	$acl=Get-acl $Sharepath\$project_id -ErrorAction Stop
	Get-NTFSAccess $Sharepath\$project_id |?{ $_.Account -notlike "BUILTIN\Administrators"} |Remove-NTFSAccess
	    
    ###############################
    ## Updated by Sanaullah Sanai
    ## Defect: GDAP-1394
    ###############################
    #Add-NTFSAccess -Account $Domain\$usernames -AccessRights FullControl -AppliesTo ThisFolderSubfoldersAndFiles -Path $Sharepath\$project_id
    #Add-NTFSAccess -Account $LabUserGroup -AccessRights FullControl -AppliesTo ThisFolderSubfoldersAndFiles -Path $Sharepath\$project_id
    ## Updated Permissions
    Add-NTFSAccess `
    -Account $Domain\$usernames `
    -AccessRights ReadData, ListDirectory, CreateFiles, CreateDirectories, AppendData, ReadExtendedAttributes, WriteExtendedAttributes,Traverse, ExecuteFile, DeleteSubdirectoriesAndFiles, ReadAttributes, WriteAttributes, Write, ReadPermissions, Read, ReadAndExecute `
    -AppliesTo ThisFolderSubfoldersAndFiles `
    -Path $Sharepath\$project_id
    Add-NTFSAccess `
    -Account $LabUserGroup `
    -AccessRights ReadData, ListDirectory, CreateFiles, CreateDirectories, AppendData, ReadExtendedAttributes, WriteExtendedAttributes,Traverse, ExecuteFile, DeleteSubdirectoriesAndFiles, ReadAttributes, WriteAttributes, Write, ReadPermissions, Read, ReadAndExecute `
    -AppliesTo ThisFolderSubfoldersAndFiles `
    -Path $Sharepath\$project_id
    ###############################
	
    Get-NTFSAccess $Sharepath\$project_id
	} Catch {
    "Warning Message: Issues with PDDS folder creation"
    "Error Details for Support: $_.Exception.Message"
    "Message to End User: Please contact your support team raising PDDS folder creation issues.."
    exit
    }  

	#Data Upload
	$Sharepath="$path\Data_Upload\Project"
	Try {
    New-Item -path $Sharepath -Name $project_id -Type directory -ErrorAction Stop
    } Catch {
    "Warning Message: Issues with PDDS folder creation"
    "Error Details for Support: $_.Exception.Message"
    "Message to End User: Please contact your support team raising PDDS folder creation issues.."
    exit
    }
	if($(Test-Path "$Sharepath\$project_id") -eq "True"){
	"Project folder has been created in PDDS Server"
	}
    Try{
	Get-item "$Sharepath\$project_id" -ErrorAction Stop | Disable-NTFSAccessInheritance
	$acl=Get-acl $Sharepath\$project_id
	Get-NTFSAccess $Sharepath\$project_id |?{ $_.Account -notlike "BUILTIN\Administrators"} |Remove-NTFSAccess
	Add-NTFSAccess -Account $Domain\$usernames -AccessRights Read,Write  -AppliesTo ThisFolderSubfoldersAndFiles -Path $Sharepath\$project_id
	Get-NTFSAccess $Sharepath\$project_id
	} Catch {
    "Warning Message: Issues with PDDS folder creation"
    "Error Details for Support: $_.Exception.Message"
    "Message to End User: Please contact your support team raising PDDS folder creation issues.."
    exit
    }  
	#Data Download
	$Sharepath="$path\Data_Download\Project"
    Try {
	New-Item -path $Sharepath -Name $project_id -Type directory -ErrorAction Stop
    } Catch {
    "Warning Message: Issues with PDDS folder creation"
    "Error Details for Support: $_.Exception.Message"
    "Message to End User: Please contact your support team raising PDDS folder creation issues.."
    exit
    }
	if($(Test-Path "$Sharepath\$project_id") -eq "True"){
	"Project folder has been created in PDDS Server"
	}
    Try{
	Get-item "$Sharepath\$project_id" -ErrorAction Stop | Disable-NTFSAccessInheritance
	$acl=Get-acl $Sharepath\$project_id -ErrorAction Stop
	Get-NTFSAccess $Sharepath\$project_id |?{ $_.Account -notlike "BUILTIN\Administrators"} |Remove-NTFSAccess
	Add-NTFSAccess -Account $Domain\$usernames -AccessRights Read,Write  -AppliesTo ThisFolderSubfoldersAndFiles -Path $Sharepath\$project_id
	Get-NTFSAccess $Sharepath\$project_id
	} Catch {
    "Warning Message: Issues with PDDS folder creation"
    "Error Details for Support: $_.Exception.Message"
    "Message to End User: Please contact your support team raising PDDS folder creation issues.."
    exit
    }  
}

Try{
Invoke-Command -Credential $psCred -ComputerName $computername -ScriptBlock ${function:create_project_directory} -ArgumentList $SharedPath,$LANID,$AnalyticsLabADGroupName,$project_name,$userdomain  -Verbose -ErrorAction Stop
} Catch {
    "Warning Message: Issues with PDDS folder creation"
    "Error Details for Support: $_.Exception.Message"
    "Message to End User: Please contact your support team raising PDDS folder creation issues.."
}

#Create User folder in PDDS

function create_user_directory($path,$usernames,$Domain)
{
#Personal
	$Sharepath = "$path\Personal"
	if( $(Test-Path "$Sharepath\$usernames") -ne  "True"){
        Try {
		    New-Item -path $Sharepath -Name $usernames -Type directory -ErrorAction Stop
        } Catch {
		    "Warning Message: Issues with PDDS folder creation"
		    "Error Details for Support: $_.Exception.Message"
		    "Message to End User: Please contact your support team raising PDDS folder creation issues.."
        
            if(!$ShakeoutFlag){
                exit
            }
        }

		Try{
            Get-item "$Sharepath\$usernames" -ErrorAction Stop | Disable-NTFSAccessInheritance
		    $acl=Get-acl $Sharepath\$usernames -ErrorAction Stop
		    Get-NTFSAccess $Sharepath\$usernames |?{ $_.Account -notlike "BUILTIN\Administrators"} |Remove-NTFSAccess
            ###############################
            ## Updated by Sanaullah Sanai
            ## Defect: GDAP-1394
            ###############################
            #Add-NTFSAccess -Account $Domain\$usernames -AccessRights FullControl -AppliesTo ThisFolderSubfoldersAndFiles -Path $Sharepath\$usernames
            ## Updated Permissions
            Add-NTFSAccess `
              -Account $Domain\$usernames `
              -AccessRights ReadData, ListDirectory, CreateFiles, CreateDirectories, AppendData, ReadExtendedAttributes, WriteExtendedAttributes,Traverse, ExecuteFile, DeleteSubdirectoriesAndFiles, ReadAttributes, WriteAttributes, Write, ReadPermissions, Read, ReadAndExecute `
              -AppliesTo ThisFolderSubfoldersAndFiles `
              -Path $Sharepath\$usernames
            ###############################
        		
		    Get-NTFSAccess $Sharepath\$usernames
        } Catch {
		    "Warning Message: Issues with PDDS folder creation"
		    "Error Details for Support: $_.Exception.Message"
		    "Message to End User: Please contact your support team raising PDDS folder creation issues.."
            if(!$ShakeoutFlag){
                
                exit
            }
            
        }  
	 } else {
        "User folder already exists in the Server."
	 }

#Data Upload
	$Sharepath="$path\Data_Upload\Personal"
	if( $(Test-Path "$Sharepath\$usernames") -ne  "True"){
        Try {
		    New-Item -path $Sharepath -Name $usernames -Type directory -ErrorAction Stop
            } Catch {
		    "Warning Message: Issues with PDDS folder creation"
		    "Error Details for Support: $_.Exception.Message"
		    "Message to End User: Please contact your support team raising PDDS folder creation issues.."
        
            if($ShakeoutFlag){
                exit
            }                    
        }
        Try{
		    Get-item "$Sharepath\$usernames" -ErrorAction Stop | Disable-NTFSAccessInheritance
		    $acl=Get-acl $Sharepath\$usernames -ErrorAction Stop
		    Get-NTFSAccess $Sharepath\$usernames |?{ $_.Account -notlike "BUILTIN\Administrators"} |Remove-NTFSAccess
		    Add-NTFSAccess -Account $Domain\$usernames -AccessRights Read,Write  -AppliesTo ThisFolderSubfoldersAndFiles -Path $Sharepath\$usernames
		    Get-NTFSAccess $Sharepath\$usernames
	    } Catch {

		    "Warning Message: Issues with PDDS folder creation"
		    "Error Details for Support: $_.Exception.Message"
		    "Message to End User: Please contact your support team raising PDDS folder creation issues.."
            
            if(!$ShakeoutFlag){

                exit
            }
        }  
	 } else {
		"User folder already exists in the Server."
	 }

    #Data Download
	$Sharepath="$path\Data_Download\Personal"
	if( $(Test-Path "$Sharepath\$usernames") -ne  "True"){
        Try{
		    New-Item -path $Sharepath -Name $usernames -Type directory -ErrorAction Stop
        } Catch {
            
            "Warning Message: Issues with PDDS folder creation"
            "Error Details for Support: $_.Exception.Message"
            "Message to End User: Please contact your support team raising PDDS folder creation issues.."
            
            if(!$ShakeoutFlag){
                exit
            }
            
         }
        
        Try{
		    Get-item "$Sharepath\$usernames" -ErrorAction Stop | Disable-NTFSAccessInheritance
		    Get-NTFSAccess $Sharepath\$usernames |?{ $_.Account -notlike "BUILTIN\Administrators"} |Remove-NTFSAccess
		    Get-NTFSAccess $Sharepath\$usernames
	    } Catch {
		    "Warning Message: Issues with PDDS folder creation"
		    "Error Details for Support: $_.Exception.Message"
		    "Message to End User: Please contact your support team raising PDDS folder creation issues.."
                
            if(!$ShakeoutFlag){
                exit
            }                
        }
          
	} else {
		"User folder already exists in the Server."
		}
} 

Try{
Invoke-Command -Credential $psCred -ComputerName $computername -ScriptBlock ${function:create_user_directory} -ArgumentList $SharedPath,$LANID,$userdomain  -Verbose -ErrorAction Stop
} Catch {
		"Warning Message: Issues with PDDS folder creation"
		"Error Details for Support: $_.Exception.Message"
		"Message to End User: Please contact your support team raising PDDS folder creation issues.."
}

#Starting second DSC for Windows

if(!$ShakeoutFlag){

    configuration PackageInstall{	
            $storageacckey = "?"+$mediastorageacckey
	        $storageaccount = $mediastorageaccount
	        $container = $mediacontainer
	        $bloburl = "https://" + $storageaccount + ".blob.core.windows.net/" + $container +"/dscmedia"

        Set-StrictMode -Off
        Import-DSCResource -ModuleName xStorage
	    Import-DscResource -ModuleName xPSDesiredStateConfiguration  
	    Import-DscResource -ModuleName PSDesiredStateConfiguration 
	    Import-DsCResource -ModuleName xPendingReboot
	

    Node $vmname {
     Registry disable_Cached_Logon
            {
               Key = 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\'
               ValueName =  'CachedLogonsCount'
               ValueData =  0
               ValueType = 'String'   
               Ensure = 'Present'
               Force = $true   
            }
      Script DisableSMB 
		    {
			    GetScript = {@{}}
			    TestScript = {
				    return $false
			    }
	
			    SetScript = {
			    Set-SmbServerConfiguration -EnableSMB1Protocol $false -Force
			    }
		
		    }
      xWaitForVolume FDrive 
	    {
		    DriveLetter = 'L'
		    RetryIntervalSec = 2
		    RetryCount = 60
        }
    	    xRemoteFile SourceTreeExe 
	    {
		    DestinationPath = ("L:\\DSCInstallables\\SourceTree\\SourceTreeSetup_1.9.6.1.exe")
		    Uri =  $bloburl + "/SourceTre/SourceTreeSetup_1.9.6.1.exe" + $storageacckey
		    DependsOn = @("[xWaitForVolume]FDrive") 
	    }
	    xRemoteFile GitExe 
	    {
		    DestinationPath = ("L:\\DSCInstallables\\SourceTree\\Git-2.11.1-64-bit.exe")
		    Uri =  $bloburl + "/Git/Git-2.11.1-64-bit.exe" + $storageacckey
		    DependsOn = @("[xWaitForVolume]FDrive") 
	    }
	    xRemoteFile Anaconda3.zip 
	    {
		    DestinationPath = ("L:\\DSCInstallables\\Anaconda3.zip")
		    Uri =  $bloburl + "/Anaconda/Anaconda3.zip" + $storageacckey
		    DependsOn = @("[xWaitForVolume]FDrive") 
	    }
	    xRemoteFile RLIB_zip 
	    {
		    DestinationPath = ("L:\\DSCInstallables\\RLib.zip")
		    Uri =  $bloburl + "/RLib.zip" + $storageacckey
		    DependsOn = @("[xWaitForVolume]FDrive") 
	    }
	    Script Anaconda3-Remove
	    {
		    GetScript = {@{}}
		    TestScript = {
			    return $false
		    }

		    SetScript = {
            & 'Remove-Item' -path L:\\ProgramFiles\\Anaconda3 -recurse -Force

		    }
		    DependsOn = @("[xWaitForVolume]FDrive")
	    }	
	    Script Anaconda3-Python # Change Start Screen Layout
	    {
		    GetScript = {@{}}
		    TestScript = {
			
				    return $false
				
		    }

		    SetScript = {
		    #powershell.exe -nologo -noprofile -command "& { Add-Type -A 'System.IO.Compression.FileSystem'; [IO.Compression.ZipFile]::ExtractToDirectory('L:\DSCInstallables\EclipseNeon\eclipse-SDK-4.6-win32-x86_64.zip', 'L:\DSCInstallables\EclipseNeon'); }"
			    #L:\DSCInstallables\unzip\unzip.exe L:\DSCInstallables\Anaconda3.zip -d L:\ProgramFiles\
			    & 'C:\Program Files\7-Zip\7z.exe' x 'L:\DSCInstallables\Anaconda3.zip' -y -o'L:\ProgramFiles\'
			    exit
		    }
		    DependsOn = @("[Script]Anaconda3-Remove","[xRemoteFile]Anaconda3.zip" ) 
	    }
	    Script RLIB_EX # Change Start Screen Layout
	    {
		    GetScript = {@{}}
		    TestScript = {
			
				    return $false
				
		    }

		    SetScript = {
		    #powershell.exe -nologo -noprofile -command "& { Add-Type -A 'System.IO.Compression.FileSystem'; [IO.Compression.ZipFile]::ExtractToDirectory('L:\DSCInstallables\EclipseNeon\eclipse-SDK-4.6-win32-x86_64.zip', 'L:\DSCInstallables\EclipseNeon'); }"
			    #L:\DSCInstallables\unzip\unzip.exe L:\DSCInstallables\RLib.zip -d L:\ProgramFiles\Microsoft\MRO-3.3.2\library\
			    & 'C:\Program Files\7-Zip\7z.exe' x 'L:\DSCInstallables\RLib.zip' -y -o'L:\ProgramFiles\Microsoft\MRO-3.3.2\library\'
			    exit
		    }
		    DependsOn = @("[xRemoteFile]RLIB_zip", "[Script]Anaconda3-Python") 
	    }		  
	    Script Git 
	    {
		    GetScript = {@{}}
		    TestScript = {
			    return $false
		    }

		    SetScript = {
            & 'L:\DSCInstallables\SourceTree\Git-2.11.1-64-bit.exe' /Silent

		    }
		    DependsOn = @("[xRemoteFile]GitExe", "[Script]RLIB_EX") 
	    }	
	    Script SourceTree
	    {
		    GetScript = {@{}}
		    TestScript = {
			    return $false
		    }

		    SetScript = {
            Start-Sleep -Seconds 90
            & 'L:\DSCInstallables\SourceTree\SourceTreeSetup_1.9.6.1.exe' /quiet       
		    }
		    DependsOn = @("[xRemoteFile]SourceTreeExe", "[Script]Git") 
	    }
		
	    Script iconRstudio
	    {
	    GetScript = {@{}}
		    TestScript = {
			    return $false
		    }

		    SetScript = {
		    $TargetFile =  "C:\\Program Files\\RStudio\\bin\\rstudio.exe"
		    $ShortcutFile = "C:\\Users\\PUBLIC\\Desktop\\RStudio.lnk"
		    $WScriptShell = New-Object -ComObject WScript.Shell
		    $Shortcut = $WScriptShell.CreateShortcut($ShortcutFile)
		    $Shortcut.TargetPath = $TargetFile
		    $Shortcut.Save()
		    }
	    }

	    xRemoteFile EclipseNeonZip 
	    {
		    DestinationPath = ("L:\\DSCInstallables\\EclipseNeon\\eclipse-SDK-4.6-win32-x86_64.zip")
            Uri =  $bloburl + "/Eclipse/eclipse-SDK-4.6-win32-x86_64.zip" + $storageacckey
		    DependsOn = @("[xWaitForVolume]FDrive") 
	    }	
	    Script UnZipEclipseNeon # Change Start Screen Layout
	    {
		    GetScript = {@{}}
		    TestScript = {
			    if (Test-Path 'L:\DSCInstallables\EclipseNeon\eclipse'){
				    return $true
				    }
			    else {
				    return $false
				    }
		    }

		    SetScript = {
		    #powershell.exe -nologo -noprofile -command "& { Add-Type -A 'System.IO.Compression.FileSystem'; [IO.Compression.ZipFile]::ExtractToDirectory('L:\DSCInstallables\EclipseNeon\eclipse-SDK-4.6-win32-x86_64.zip', 'L:\DSCInstallables\EclipseNeon'); }"
			    L:\DSCInstallables\unzip\unzip.exe L:\DSCInstallables\EclipseNeon\eclipse-SDK-4.6-win32-x86_64.zip -d L:\DSCInstallables\EclipseNeon
			
			    exit
		    }
		    DependsOn = @("[xRemoteFile]EclipseNeonZip") 
	    }
	
	    Script iconEclipseNeon
	    {
	    GetScript = {@{}}
		    TestScript = {
			    return $false
		    }

		    SetScript = {
		    $TargetFile =  "L:\\DSCInstallables\\EclipseNeon\\eclipse\\eclipse.exe"
		    $ShortcutFile = "C:\\Users\\PUBLIC\\Desktop\\eclipse.lnk"
		    $WScriptShell = New-Object -ComObject WScript.Shell
		    $Shortcut = $WScriptShell.CreateShortcut($ShortcutFile)
		    $Shortcut.TargetPath = $TargetFile
		    $Shortcut.Save()
		    }
		    DependsOn = @("[Script]UnZipEclipseNeon") 
	    }
	    #putty 
	    xRemoteFile PUTTY-MSI 
	    {
		    DestinationPath = ("L:\\DSCInstallables\\PUTTY\\putty-64bit-0.69-installer.msi")
		
            Uri =  $bloburl + "/PUTTY/putty-64bit-0.69-installer.msi" + $storageacckey
		    DependsOn = @("[xWaitForVolume]FDrive") 
	    }
	    Package Putty_install
        {
            Ensure = "Present"
            Name = "PuTTY release 0.69 (64-bit)"
            Path = "L:\\DSCInstallables\\PUTTY\\putty-64bit-0.69-installer.msi"
            ProductId = ''
		    Arguments = ' /quiet '
		    DependsOn =   @("[xRemoteFile]PUTTY-MSI") 
        }
	    #DB 2
	    xRemoteFile DB2-ZIP 
	    {
		    DestinationPath = ("L:\\DSCInstallables\\DB2\\ntx64_odbc_cli.zip")
		
            Uri =  $bloburl + "/DB2/v10.5fp8_ntx64_odbc_cli.zip" + $storageacckey
		    DependsOn = @("[xWaitForVolume]FDrive") 
	    }
	
	    Archive DB2{
	    Ensure = "Present"
	    Path = "L:\\DSCInstallables\\DB2\\ntx64_odbc_cli.zip"
	    Destination = "C:\\IBMDB2\\"
	    DependsOn = @("[xRemoteFile]DB2-ZIP") 
	
	    }
	
	    xRemoteFile DB2-ConfigFile 
	    {
		    DestinationPath = ("L:\\DSCInstallables\\DB2\\db2_config.zip")
		
            Uri =  $bloburl + "/DB2/db2_config.zip" + $storageacckey
		    DependsOn = @("[xWaitForVolume]FDrive") 
	    }
	
	    Archive DB2cfg{
	    Ensure = "Present"
	    Path = "L:\\DSCInstallables\\DB2\\db2_config.zip"
	    Destination = "C:\\IBMDB2\\clidriver\\cfg\\"
	    DependsOn = @("[xRemoteFile]DB2-ConfigFile") 
	
	    }
	    Environment DB2_HOME
	    {
	    Ensure = "Present"
	    Name = "IBM_DB_HOME"
	    Value="c:\IBMDB2\clidriver\"
	    }
	
	    Environment DB2_DRIVER_CFG
	    {
	    Ensure = "Present"
	    Name = "DB2DSDRIVER_CFG_PATH"
	    Value="c:\IBMDB2\clidriver\cfg\"
	    }
	    Environment DB2_CLIINIPATH
	    {
	    Ensure = "Present"
	    Name = "DB2CLIINIPATH"
	    Value="c:\IBMDB2\clidriver\cfg\"
	    }
	
	    Script driver_register
	    {
		    GetScript = {@{}}
		    TestScript = {
			    return $false
		    }

		    SetScript = {
            & 'c:\IBMDB2\clidriver\bin\db2cli.exe' install -setup

		    }
		    DependsOn = @("[Environment]DB2_CLIINIPATH")
	    }
	
	    xRemoteFile java8exe 
	    {
		    DestinationPath = ("L:\\DSCInstallables\\Java8\\jdk-8u121-windows-x64.exe")
		
            Uri =  $bloburl + "/JDK/jdk-8u121-windows-x64.exe" + $storageacckey
		    DependsOn = @("[xWaitForVolume]FDrive") 
	    }		
	    Package Java8
        {
            Ensure = "Present"
            Name = "Java SE Development Kit 8 Update 121 (64-bit)"
            Path = "L:\\DSCInstallables\\Java8\\jdk-8u121-windows-x64.exe"
            ProductId = '64A3A4F4-B792-11D6-A78A-00B0D0180121'
		    Arguments = ' /s ADDLOCAL="ToolsFeature,SourceFeature,PublicjreFeature" INSTALLDIR="L:\ProgramFiles\Java\jdk1.8.0_121" /INSTALLDIRPUBJRE="L:\ProgramFiles\Java\jre1.8.0_121"'
		    DependsOn =   @("[xRemoteFile]java8exe") 
        }
      }
    }
} 
#End of DSC install 


PackageInstall

$vmstatus=((Get-AzureRmVM -ResourceGroupName $Resourcegroupname -Name $vmname  -Status | Select-Object -Property Statuses).statuses|Where-Object {$_.Code -like 'PowerState/*'}).DisplayStatus
Write-Output "The status of the VM $vmname is $vmstatus"
$count=0
while (($vmstatus -ne "VM running") -and ($count -le 5))
{
    $count += 1
    $vmstatus=((Get-AzureRmVM -ResourceGroupName $Resourcegroupname -Name $vmname  -Status | Select-Object -Property Statuses).statuses|Where-Object {$_.Code -like 'PowerState/*'}).DisplayStatus
    sleep 30
    "Waiting for VM to restart"
}

sleep 300

$domainjoinflag = 'checking'

Try{
    Get-ADComputer -Identity $vmname -Credential $Mycred -Server $domainToJoin -ErrorAction Stop
    Write-Output "The Computer $vmname has successfully joined to the domain $domaintojoin"
    $domainjoinflag = "yes"
} Catch {
    $k = 1
    do {
        "Waiting for Computer to sync with QBE AD.."
        $k++
        sleep 120
    } while($k -le 6)
}

if($domainjoinflag -ne "yes"){
    Try{
    Get-ADComputer -Identity $vmname -Credential $Mycred -Server $domainToJoin -ErrorAction Stop
    } Catch {
        "Computer didn't join to QBE AD..."
	    "Trying a restart to resolve hostname.."
	    Restart-AzureRmVM -ResourceGroupName $Resourcegroupname -Name $vmname -ErrorAction Stop -WarningAction SilentlyContinue
	    Write-Output "$vmname restarted to resolve hostname resolution"
    }
} else {
 Write-Output "Initiating second DSC installation"
}


if($domainjoinflag -ne "yes"){
    $vmstatus=((Get-AzureRmVM -ResourceGroupName $Resourcegroupname -Name $vmname  -Status | Select-Object -Property Statuses).statuses|Where-Object {$_.Code -like 'PowerState/*'}).DisplayStatus
    $count=0
    while (($vmstatus -ne "VM running") -and ($count -le 5))
    {
    $count += 1
    $vmstatus=((Get-AzureRmVM -ResourceGroupName $Resourcegroupname -Name $vmname  -Status | Select-Object -Property Statuses).statuses|Where-Object {$_.Code -like 'PowerState/*'}).DisplayStatus
    sleep 30
    "Waiting for VM to restart.."
    }
} else {
 Write-Output "Initiating second DSC installation.."
}

if($domainjoinflag -ne "yes"){
    Try{
    Get-ADComputer -Identity $vmname -Credential $Mycred -Server $domainToJoin -ErrorAction Stop
    } Catch {
        "Warning Message: Computer didn't join to QBE AD.."
        "Error Details for Support: $_.Exception.Message"
	    "Message to End User: Please contact your support team for manual execution of second DSC"
        exit
    }
} else {
 Write-Output "Initiating second DSC installation.."
}


Try{
Test-WSMan -ComputerName $vmname -ErrorAction Stop
} Catch {
$z = 1 
do{
    "Waiting for WinRM Services to start.."
    $z++
    sleep 120
        }while($z -le 8)
}



if((Test-WSMan -ComputerName $vmname -ErrorAction Stop).wsmid -ne $null){
"Starting second DSC installation.."
} else {
    "Warning Message: Failed to start WinRM services"
    "Error Details for Support: $_.Exception.Message"
	"Message to End User: Please contact your support team for manual execution of second DSC"
    exit
}


$Pass= ConvertTo-SecureString $adminPassword -AsPlainText -Force
$credential = New-Object System.Management.Automation.PSCredential(".\$adminUsername",$Pass)

Try{
Start-DscConfiguration -Path .\PackageInstall  -ComputerName $vmname  -Credential $credential -Force -Verbose -ErrorAction Stop -wait
} Catch {
    "Warning Message: Failed to executed DSC for secondary tool installations"
    "Error Details for Support: $_.Exception.Message"
	"Message to End User: Please contact your support team for manual execution of second DSC"
    exit
}

$Session = New-CimSession -ComputerName $vmname -Credential $credential

sleep 60

Try {
$m = 1
do{
"Waiting for second DSC to finish.."
$m++
sleep 60
} while (((Get-DscConfigurationstatus -CimSession $Session -ErrorAction Stop).Status -ne "Success") -and ($m -le 5))
} Catch {
     if ($_.Exception.Message -like "*Cannot invoke the Get-DscConfigurationStatus cmdlet. The Start-DscConfiguration cmdlet is in progress and must return before Get-DscConfigurationStatus can be invoked*"){
        "Second DSC is still running"
        exit} else {
		"Warning Message: Failed to complete DSC for secondary tool installations"
		"Error Details for Support: $_.Exception.Message"
		"Message to End User: Please contact your support team for manual execution of second DSC"
		 exit
        }
}

exit -128